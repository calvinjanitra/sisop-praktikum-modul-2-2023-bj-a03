#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stddef.h>

char* getTimestamp(){
    time_t cur = time(NULL);
    char* timestamp = malloc(20 * sizeof(char));
    struct tm* cur_time = localtime(&cur);

    strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", cur_time);

    return timestamp;
}

char* create_folder(char* timestamp){
    char *folder_name = malloc(30 * sizeof(char));
    sprintf(folder_name, "%s", timestamp);
    
    pid_t make_folder = fork();
    if(make_folder == 0){
        char *args[] ={"mkdir", timestamp, NULL};
        execv("/bin/mkdir", args);
    }
    else{
        wait(NULL);
    }
    return folder_name;
}

void download_file_and_zip(){
    char *timestamp = getTimestamp();
    char *folder_name = create_folder(timestamp);

    pid_t child = fork();
    int status;
    if(child == 0){
        for(int i=0; i<15; i++){
            char filename[200];
            char *file_timestamp = getTimestamp();
            sprintf(filename, "%s/%s.jpg", folder_name, file_timestamp);

            char url[100];
            time_t cur = time(NULL);
            int image_size = (cur%1000)+50;
            sprintf(url, "https://picsum.photos/%d", image_size);
            pid_t download = fork();
            if(download == 0){
                char *args[] ={"wget", "-qO", filename, url, NULL};
                execv("/usr/bin/wget", args);
                printf("Failed\n");
            }
            else{
                wait(&status);
            }

            sleep(5);
        }

        pid_t zip = fork();

        if(zip == 0){
            char zip_file[100];
            sprintf(zip_file, "%s.zip", folder_name);
            char *args[] = {"zip", zip_file, folder_name, NULL};
            execv("/usr/bin/zip", args);
        }
        else{
            wait(&status);
        }

        pid_t remove_folder = fork();
        if(remove_folder == 0){
            char *args[] = {"rmdir", "-r", folder_name, NULL};
            execv("/bin/rm", args);
        }
        else{
            wait(&status);
        }
    }
    else{
        wait(&status);
    }
}

void generate_killer(int argc, char *argv[], int parent_id){
    int status;
    FILE *pkiller = fopen("killer.c", "w");
    char string_format[] = "%s";

    fprintf(pkiller, "#include <stdio.h>\n#include <stdlib.h>\n#include <time.h>\n#include <unistd.h>\n#include <sys/types.h>\n#include <string.h>\n#include <sys/wait.h>\n#include <sys/stat.h>\n");
    fprintf(pkiller, "int main(int argc, int *argv[]){\n");
    fprintf(pkiller, "pid_t child = fork();\n"
                    "if(child == 0){\n");
    fprintf(pkiller, "if(strcmp(argv[1], \"-a\") == 0){char *args[] = {\"pkill\", \"-9\", \"lukisan\", NULL};\n"
                    "execv(\"/usr/bin/pkill\", args);}\n");

    fprintf(pkiller, "else if(strcmp(argv[1], \"-b\") == 0){char *args[] = {\"kill\", \"-9\", \"%d\", NULL};\n", parent_id);
    fprintf(pkiller, "execv(\"/bin/kill\", args);}\n}");
    fprintf(pkiller, "}\n");

    fclose(pkiller);
}

void run_killer(int argc, char *argv[]){
    int status;
    pid_t child = fork();
    if(child == 0){
        char *args[] = {"gcc", "killer.c", "-o", "killer"};
        execv("/usr/bin/gcc", args);
    }
    else{
        wait(&status);
    }
    pid_t run_output = fork();    
    if(run_output == 0){
        char *args[] = {argv[0], argv[1], NULL};
        execv("./killer", args);
    }
    else{
        wait(&status);
    }

    pid_t delete = fork();
    if(delete == 0){
        char *args[] = {"rm", "killer.c", NULL};
        execv("/bin/rm", args);
    }
    else{
        char *args[] = {"rm", "killer", NULL};
        execv("/bin/rm", args);
    }
}

int main(int argc, char *argv[]){

    if(argc == 1){
        pid_t child = fork();
        if(child == 0){
            generate_killer(argc, argv, getpid());
            while (1){
                pid_t download_zip= fork();
                if(download_zip == 0){
                    download_file_and_zip();
                    exit(EXIT_SUCCESS);
                }
                else{
                    sleep(30);
                }
            }
        }
        else{
            exit(EXIT_SUCCESS);
        }
    }

    else if(argc == 2 && (strcmp(argv[1], "-a") == 0) || (strcmp(argv[1], "-b") == 0)){
        run_killer(argc, argv);
        return 0;
    }   
}


