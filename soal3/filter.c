#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <curl/curl.h>
#include <string.h>
#include <wait.h>

typedef struct node {
	char data[100];

	// Lower values indicate higher priority
	int priority;

	struct node* next;

} Node;

// Function to Create A New Node
Node* newNode(char *d, int p)
{
	Node* temp = (Node*)malloc(sizeof(Node));
	//temp->data = d;
    strcpy(temp->data, d);
	temp->priority = p;
	temp->next = NULL;

	return temp;
}

// Return the value at head
const char* peek(Node** head)
{
	return (*head)->data;
}

// Removes the element with the
// highest priority from the list
void pop(Node** head)
{
	Node* temp = *head;
	(*head) = (*head)->next;
	free(temp);
}

// Function to push according to priority
void push(Node** head, char *d, int p)
{
	Node* start = (*head);

	// Create new Node
	Node* temp = newNode(d, p);
    
	// Special Case: The head of list has lesser
	// priority than new node. So insert new
	// node before head node and change head node.
	if ((*head)->priority < p) {

		// Insert New Node before head
		temp->next = *head;
		(*head) = temp;
	}
	else {

		// Traverse the list and find a
		// position to insert new node
		while (start->next != NULL &&
			start->next->priority > p) {
			start = start->next;
		}

		// Either at the ends of the list
		// or at required position
		temp->next = start->next;
		start->next = temp;
	}
}

// Function to check is list is empty
int isEmpty(Node** head)
{
	return (*head) == NULL;
}

void manutd (){
    DIR *dp;
    struct dirent *ep;
    char path[] = "/home/dafarelcky/Prak_2_sisop/players";
    char filename[50];
    dp = opendir(path);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            char path[] = "/home/dafarelcky/Prak_2_sisop/players/";
            strcpy(filename, ep->d_name);
            strncat(path, filename, 50);
          //  printf("%s\n", path);
            if (strstr(path, "ManUtd") == NULL){
            //    printf("Ada ");
                if(strstr(path, "png") != NULL) remove(path);
            }
          //  puts(ep->d_name);
        } 
    (void) closedir (dp);
    } else perror ("Couldn't open the directory");    
}

void kategori (){
    DIR *dp;
    struct dirent *ep;
    char path_a[] = "/home/dafarelcky/Prak_2_sisop/players";
    char filename[200];
    FILE *fp1, *fp2;
    int ch;
    char end_path[300];
  //  char source[256], target[256];
	// Creating first child
	int n1 = fork();

	// Creating second child. First child
	// also executes this line and creates
	// grandchild.
	int n2 = fork();
    pid_t child_id;
    int status;
	if (n1 > 0 && n2 > 0) {
        child_id = fork();

        if(child_id == 0){
            char *argv[] = {"mkdir", "/home/dafarelcky/Prak_2_sisop/players/Kiper", NULL};
            execv("/usr/bin/mkdir", argv);
        }
        else {
            while ((wait(&status)) > 0);
            dp = opendir(path_a);
            if (dp != NULL)
            {
                
                while ((ep = readdir (dp))) {
                int len = strlen(ep->d_name);//get the length of input string
                int des = strlen("/home/dafarelcky/Prak_2_sisop/players/Kiper/");
                int ph = strlen("/home/dafarelcky/Prak_2_sisop/players/");
                //then size your input array accordingly using `sum of consecutive sequence` 
                size_t size = (len*(len + 1)/2) + 1;//adding one for final null terminator
                size_t des_size = (des*(des + 1)/2) + 1;
                size_t path_size = (ph*(ph + 1)/2) + 1;
                char filename[size]; //using VLA (Variable Length Array)
                char des_path[des_size];
                char path[path_size];
                strcpy(des_path, "/home/dafarelcky/Prak_2_sisop/players/Kiper/");
                strcpy(path, "/home/dafarelcky/Prak_2_sisop/players/");
                strcpy(filename, ep->d_name);
                strncat(path, filename, strlen(ep->d_name));
                strncat(des_path, filename, strlen(ep->d_name));

                if (strstr(ep->d_name, "Kiper") != NULL && strstr(ep->d_name, "png")!= NULL){
                  // printf("%s\n", path);
                    fp1 = fopen(path, "rb");
                    fp2 = fopen(des_path, "wb");

                    while ((ch = fgetc(fp1)) != EOF) {
                       // printf("%d\n", ch);
                        fputc(ch, fp2);
                    }
                    fclose(fp1);
                    fclose(fp2);
                    remove(path);
                }
               // puts(ep->d_name);
                } 
                (void) closedir (dp);
            } else perror ("Couldn't open the directory");
        }
	}
	else if (n1 == 0 && n2 > 0)
	{
		child_id = fork();

        if(child_id == 0){
            char *argv[] = {"mkdir", "/home/dafarelcky/Prak_2_sisop/players/Bek", NULL};
            execv("/usr/bin/mkdir", argv);
        }
        else {
            while ((wait(&status)) > 0);
            dp = opendir(path_a);
            if (dp != NULL)
            {
                
                while ((ep = readdir (dp))) {
                int len = strlen(ep->d_name);//get the length of input string
                int des = strlen("/home/dafarelcky/Prak_2_sisop/players/Bek/");
                int ph = strlen("/home/dafarelcky/Prak_2_sisop/players/");
                //then size your input array accordingly using `sum of consecutive sequence` 
                size_t size = (len*(len + 1)/2) + 1;//adding one for final null terminator
                size_t des_size = (des*(des + 1)/2) + 1;
                size_t path_size = (ph*(ph + 1)/2) + 1;
                char filename[size]; //using VLA (Variable Length Array)
                char des_path[des_size];
                char path[path_size];
                strcpy(des_path, "/home/dafarelcky/Prak_2_sisop/players/Bek/");
                strcpy(path, "/home/dafarelcky/Prak_2_sisop/players/");
                strcpy(filename, ep->d_name);
                strncat(path, filename, strlen(ep->d_name));
                strncat(des_path, filename, strlen(ep->d_name));

                if (strstr(ep->d_name, "Bek") != NULL && strstr(ep->d_name, "png")!= NULL){
                  // printf("%s\n", path);
                    fp1 = fopen(path, "rb");
                    fp2 = fopen(des_path, "wb");

                    while ((ch = fgetc(fp1)) != EOF) {
                       // printf("%d\n", ch);
                        fputc(ch, fp2);
                    }
                    fclose(fp1);
                    fclose(fp2);
                    remove(path);
                }
               // puts(ep->d_name);
                } 
                (void) closedir (dp);
            } else perror ("Couldn't open the directory");
        }
	}
	else if (n1 > 0 && n2 == 0)
	{
		child_id = fork();

        if(child_id == 0){
            char *argv[] = {"mkdir", "/home/dafarelcky/Prak_2_sisop/players/Gelandang", NULL};
            execv("/usr/bin/mkdir", argv);
        }
        else {
            while ((wait(&status)) > 0);
            dp = opendir(path_a);
            if (dp != NULL)
            {
                
                while ((ep = readdir (dp))) {
                int len = strlen(ep->d_name);//get the length of input string
                int des = strlen("/home/dafarelcky/Prak_2_sisop/players/Gelandang/");
                int ph = strlen("/home/dafarelcky/Prak_2_sisop/players/");
                //then size your input array accordingly using `sum of consecutive sequence` 
                size_t size = (len*(len + 1)/2) + 1;//adding one for final null terminator
                size_t des_size = (des*(des + 1)/2) + 1;
                size_t path_size = (ph*(ph + 1)/2) + 1;
                char filename[size]; //using VLA (Variable Length Array)
                char des_path[des_size];
                char path[path_size];
                strcpy(des_path, "/home/dafarelcky/Prak_2_sisop/players/Gelandang/");
                strcpy(path, "/home/dafarelcky/Prak_2_sisop/players/");
                strcpy(filename, ep->d_name);
                strncat(path, filename, strlen(ep->d_name));
                strncat(des_path, filename, strlen(ep->d_name));

                if (strstr(ep->d_name, "Gelandang") != NULL && strstr(ep->d_name, "png")!= NULL){
                  // printf("%s\n", path);
                    fp1 = fopen(path, "rb");
                    fp2 = fopen(des_path, "wb");

                    while ((ch = fgetc(fp1)) != EOF) {
                       // printf("%d\n", ch);
                        fputc(ch, fp2);
                    }
                    fclose(fp1);
                    fclose(fp2);
                    remove(path);
                }
               // puts(ep->d_name);
                } 
                (void) closedir (dp);
            } else perror ("Couldn't open the directory");
        }
	}
	else {
		child_id = fork();

        if(child_id == 0){
            char *argv[] = {"mkdir", "/home/dafarelcky/Prak_2_sisop/players/Penyerang", NULL};
            execv("/usr/bin/mkdir", argv);
        }
        else {
            while ((wait(&status)) > 0);
            dp = opendir(path_a);
            if (dp != NULL)
            {
                
                while ((ep = readdir (dp))) {
                int len = strlen(ep->d_name);//get the length of input string
                int des = strlen("/home/dafarelcky/Prak_2_sisop/players/Penyerang/");
                int ph = strlen("/home/dafarelcky/Prak_2_sisop/players/");
                //then size your input array accordingly using `sum of consecutive sequence` 
                size_t size = (len*(len + 1)/2) + 1;//adding one for final null terminator
                size_t des_size = (des*(des + 1)/2) + 1;
                size_t path_size = (ph*(ph + 1)/2) + 1;
                char filename[size]; //using VLA (Variable Length Array)
                char des_path[des_size];
                char path[path_size];
                strcpy(des_path, "/home/dafarelcky/Prak_2_sisop/players/Penyerang/");
                strcpy(path, "/home/dafarelcky/Prak_2_sisop/players/");
                strcpy(filename, ep->d_name);
                strncat(path, filename, strlen(ep->d_name));
                strncat(des_path, filename, strlen(ep->d_name));

                if (strstr(ep->d_name, "Penyerang") != NULL && strstr(ep->d_name, "png")!= NULL){
                  // printf("%s\n", path);
                    fp1 = fopen(path, "rb");
                    fp2 = fopen(des_path, "wb");

                    while ((ch = fgetc(fp1)) != EOF) {
                       // printf("%d\n", ch);
                        fputc(ch, fp2);
                    }
                    fclose(fp1);
                    fclose(fp2);
                    remove(path);
                }
               // puts(ep->d_name);
                } 
                (void) closedir (dp);
            } else perror ("Couldn't open the directory");
        }
	}
}

void buatTim(int Bek, int Gelandang, int Penyerang){
    FILE *fp;
    char name[] = "/home/dafarelcky/Formasi_";
    char temp[1];
    printf("%s\n", name);
    sprintf(temp, "%d", Bek);
    strcat(name, temp);
    printf("%s\n", name);
    strcat(name, "-");
    printf("%s\n", name);
    sprintf(temp, "%d", Gelandang);
    strcat(name, temp);
    printf("%s\n", name);
    strcat(name, "-");
    printf("%s\n", name);
    sprintf(temp, "%d", Penyerang);
    strcat(name, temp);
    printf("%s\n", name);
    strcat(name, ".txt");
    printf("%s\n", name);
    fp = fopen(name, "w");
    Node* pq = newNode("fill", 0);
    DIR *dp;
    struct dirent *ep;
    char path[] = "/home/dafarelcky/Prak_2_sisop/players/Kiper";
    char filename[50];
    dp = opendir(path);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
           // char path[] = "/home/dafarelcky/Prak_2_sisop/players/Kiper";
            strcpy(filename, ep->d_name);
            char stat[2];
            int c = 0;
            for (int i = strlen(filename)-6; i < strlen(filename) - 4; i++)
            {
                stat[c] = filename[i];
           //     printf("%c %d\n", f[i], i);
                c++;
            }
            stat[c] = '\0';
            int num_stat = atoi(stat);
          //  filename[strlen(filename) - 4] = '\0';
            strcat(filename, "\n");
           // printf("%s %d\n", filename, num_stat);
            if (strstr(filename, "png")!= NULL) push(&pq, filename, num_stat);
        
          //  strncat(path, filename, 50);
          //  printf("%s\n", path);
            
          //  puts(ep->d_name);
        } 
        // printf("%s\n", peek(&pq)); 
        //  printf("%s\n", peek(&pq)); 
       // pop(&pq);
        fputs(peek(&pq), fp);
        while (!isEmpty(&pq)) {
		 //   printf("%s\n", peek(&pq));
		    pop(&pq);
	    }
    (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    Node *pq_b = newNode("fill", 0);
   // DIR *dp;
   // struct dirent *ep;
    char path_b[] = "/home/dafarelcky/Prak_2_sisop/players/Bek";
    char filename_b[50];
    dp = opendir(path_b);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
           // char path[] = "/home/dafarelcky/Prak_2_sisop/players/Kiper";
            strcpy(filename_b, ep->d_name);
            char stat[2];
            int c = 0;
            for (int i = strlen(filename_b)-6; i < strlen(filename_b) - 4; i++)
            {
                stat[c] = filename_b[i];
           //     printf("%c %d\n", f[i], i);
                c++;
            }
            stat[c] = '\0';
            int num_stat = atoi(stat);
           // filename_b[strlen(filename_b) - 4] = '\0';
            strcat(filename_b, "\n");
           // printf("%s %d\n", filename, num_stat);
            if (strstr(filename_b, "png")!= NULL) push(&pq_b, filename_b, num_stat);
        
          //  strncat(path, filename, 50);
          //  printf("%s\n", path);
            
          //  puts(ep->d_name);
        } 
        // printf("%s\n", peek(&pq)); 
        //  printf("%s\n", peek(&pq)); 
       // pop(&pq);
       for (int i = 0; i < Bek; i++)
       {
            fputs(peek(&pq_b), fp);
            pop(&pq_b);
       }
        // while (!isEmpty(&pq_b)) {
		//     pop(&pq_b);
	    // }
    (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    Node* pq_g = newNode("fill", 0);
    // DIR *dp;
    // struct dirent *ep;
    char path_g[] = "/home/dafarelcky/Prak_2_sisop/players/Gelandang";
    char filename_g[50];
    dp = opendir(path_g);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
           // char path[] = "/home/dafarelcky/Prak_2_sisop/players/Kiper";
            strcpy(filename_g, ep->d_name);
            char stat[2];
            int c = 0;
            for (int i = strlen(filename_g)-6; i < strlen(filename_g) - 4; i++)
            {
                stat[c] = filename_g[i];
           //     printf("%c %d\n", f[i], i);
                c++;
            }
            stat[c] = '\0';
            int num_stat = atoi(stat);
          //  filename_g[strlen(filename_g) - 4] = '\0';
           strcat(filename_g, "\n");
            if (strstr(filename_g, "png")!= NULL) push(&pq_g, filename_g, num_stat);
        
          //  strncat(path, filename, 50);
          //  printf("%s\n", path);
            
          //  puts(ep->d_name);
        } 
        // printf("%s\n", peek(&pq)); 
        //  printf("%s\n", peek(&pq)); 
       // pop(&pq);
        for (int i = 0; i < Gelandang; i++)
       {
            fputs(peek(&pq_g), fp);
            pop(&pq_g);
       }
      // pop(&pq_g);
        while (!isEmpty(&pq_g)) {
		    pop(&pq_g);
	    }
    (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    Node *pq_p = newNode("fill", 0);
    // DIR *dp;
    // struct dirent *ep;
    char path_p[] = "/home/dafarelcky/Prak_2_sisop/players/Penyerang";
    char filename_p[50];
    dp = opendir(path_p);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
           // char path[] = "/home/dafarelcky/Prak_2_sisop/players/Kiper";
            strcpy(filename_p, ep->d_name);
            char stat[2];
            int c = 0;
            for (int i = strlen(filename_p)-6; i < strlen(filename_p) - 4; i++)
            {
                stat[c] = filename_p[i];
           //     printf("%c %d\n", f[i], i);
                c++;
            }
            stat[c] = '\0';
            int num_stat = atoi(stat);
           // filename_p[strlen(filename_p) - 4] = '\0';
            strcat(filename_p, "\n");
            
            if (strstr(filename_p, "png")!= NULL) {
               // printf("%s %d\n", filename_p, num_stat);
               // printf("%s\n", peek(&pq));
                push(&pq_p, filename_p, num_stat);
              //  printf("%s\n", peek(&pq_p));
                
            }
          //  strncat(path, filename, 50);
          //  printf("%s\n", path);
            // printf("%s\n", peek(&pq_p));
          //  puts(ep->d_name);
        } 
        // printf("%s\n", peek(&pq)); 
        //  printf("%s\n", peek(&pq)); 
       // pop(&pq);
        for (int i = 0; i < Penyerang; i++)
       {
            fputs(peek(&pq_p), fp);
            
            pop(&pq_p);
            //printf("%d\n", i);
       }
        // while (!isEmpty(&pq)) {
        //     printf("%s\n", peek(&pq));
		//     pop(&pq);
	    // }
    (void) closedir (dp);
    } else perror ("Couldn't open the directory");   
    fclose(fp); 
}

int main(){
    // no 3a
    CURL *curl;
    FILE *fp;
    CURLcode res;
    char url[] = "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF";
    // https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF
    // curl -L -o players.zip "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF"
    char outfilename[] = "players.zip";

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(outfilename,"wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        curl_easy_cleanup(curl);
        fclose(fp);
    }
    pid_t child_id;
    int status;
    child_id = fork();

    if(child_id == 0){
        char *argv[] = {"unzip", "/home/dafarelcky/Prak_2_sisop/players.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    else {
        while ((wait(&status)) > 0);
        child_id = fork();
        if(child_id == 0){
        char *argv[] = {"rm", "/home/dafarelcky/Prak_2_sisop/players.zip", NULL};
        execv("/usr/bin/rm", argv);
        }
        else {
            while ((wait(&status)) > 0);
            manutd();
            kategori();
            buatTim(4, 3, 3);
        }
    }
}