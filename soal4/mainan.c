#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <ctype.h>


int main(int argc, char *argv[]){
    if(argc != 5){
        printf("Argument are not valid\n");
        exit(EXIT_FAILURE);
    }

    int hour = 0;  
    int min = 0;
    int sec = 0;

    char star_h[2] = " ";
    char star_m[2] = " ";
    char star_s[2] = " ";


    if(strcmp(argv[1], "*") == 0){
        strcpy(star_h, "*");
    }
    else if(isdigit(argv[1][0]) && (atoi(argv[1]) >= 0 &&  atoi(argv[1]) <= 23)){
        hour = atoi(argv[1]);
    }
    else{
        printf("Invalid hour\n");
        exit(EXIT_FAILURE);
    }
   

   if(strcmp(argv[2], "*") == 0 ){
        strcpy(star_m, "*");
    }
    else if(isdigit(argv[2][0]) && atoi(argv[2]) >= 0 &&  atoi(argv[2]) <= 59){
        min = atoi(argv[2]);
    }
    else{
        printf("Invalid minute\n");
        exit(EXIT_FAILURE);
    }

    if(strcmp(argv[3], "*") == 0 ){
        strcpy(star_s, "*");
    }
    else if(isdigit(argv[3][0]) && (atoi(argv[3]) >= 0 &&  atoi(argv[3]) <= 59)){
        sec = atoi(argv[3]);
   }
    else{
        printf("Invalid second\n");
        exit(EXIT_FAILURE);
    }

    if(access(argv[4], F_OK) == 0){
        if(strstr(argv[4], ".sh") == NULL){
            printf("File is not a bash file\n");
            exit(EXIT_FAILURE);
        }
    }

    else if(access(argv[4], F_OK) != 0){
        if(errno == ENOENT){
            printf("File doesnt exist\n");
        }
        else if(errno == EACCES){
            printf("File unaccessable\n");
        }
        else{
            printf("Unknown error\n");
        }
        exit(EXIT_FAILURE);
    }

    time_t current_time = time(NULL);
    struct tm* local_time = localtime(&current_time);

    int cur_hour = local_time->tm_hour;
    int cur_min = local_time->tm_min;
    int cur_sec = local_time->tm_sec;


    int sleep_time = 0;
    

    if(strcmp(star_h, "*") == 0){
        if(strcmp(star_m, "*") == 0){
            if(strcmp(star_s, "*") != 0){
                sleep_time = sec - cur_sec;

                 if(sec < cur_sec){
                    sleep_time += 60;
                 }
            }
        }
        else{
            sleep_time = min * 60 + sec - (cur_min * 60 + cur_sec);
            if(min < cur_min){
                sleep_time += 3600;
            }
        }
    }

    else{
        sleep_time = (hour * 3600 + min * 60 + sec) - (cur_hour * 3600 + cur_min * 60 + cur_sec);
        if(hour < cur_hour){
            sleep_time += (24 * 3600);
        }
    }

    if(sleep_time < 0){
        sleep_time = 0;
    }

    bool first = true;


    pid_t child = fork();
    if(child == -1){
        exit(EXIT_FAILURE);
    }
    else if(child == 0){
        sleep(sleep_time);
        while(1){
            current_time = time(NULL);
            local_time = localtime(&current_time);

            cur_hour = local_time->tm_hour;
            cur_min = local_time->tm_min;
            cur_sec = local_time->tm_sec;

            int status;

            
            pid_t child_id  = fork();
            if(child_id == -1){
                exit(EXIT_FAILURE);
                }

                else if(child_id == 0){
                    printf("Current time : %d %d %d\n", cur_hour, cur_min, cur_sec);
                    if((hour == cur_hour || strcmp(star_h, "*") == 0 ) && (min == cur_min || strcmp(star_m, "*") == 0 ) && (sec == cur_sec || strcmp(star_s, "*") == 0 )){
                        char *args[] = {"/bin/bash", argv[4], NULL};
                        execvp("/bin/bash", args);
                    }

                }
            else{
                wait(&status);
            }

            sleep_time = 0;

            if(strcmp(star_h, "*") != 0 ){
                sleep_time += (24* 3600);
            }

            else if(strcmp(star_m, "*") != 0 ){
                sleep_time += 3600;
            }

            else if(strcmp(star_s, "*") != 0 ){
                sleep_time += 60;
            }
            else{
                sleep_time = 1;
            }

            if(first == true){
                if(strcmp(star_h, "*") != 0){
                    if(strcmp(star_m, "*") == 0){ 

                        
                        sleep_time -= cur_min*60;
                    }
                    if(strcmp(star_s, "*") == 0){
                        sleep_time -= cur_sec;
                    }
                }
                else{
                    if(strcmp(star_m, "*") != 0){
                        if(strcmp(star_s, "*") == 0){
                            sleep_time -= cur_sec;
                        }
                    }
                }
                first = false;
            }
            sleep(sleep_time);
        }
    }

    else{
        exit(EXIT_SUCCESS);
    }
    
}