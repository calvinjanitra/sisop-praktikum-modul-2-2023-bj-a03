#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <sys/wait.h>

void download(char *url, char *output)
{
  int status;
  pid_t download_id = fork();

  if (download_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (download_id == 0)
  {
    char *args[] = {"wget",url, "-q", "-O", output, NULL};
    execv("/bin/wget", args);
  }
  wait(&status);
}

void unzip(char *input)
{
  int status;
  pid_t unzip_id = fork();

  if (unzip_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (unzip_id == 0)
  {
    char *args[] = {"unzip", "-q", input, NULL};
    execv("/bin/unzip", args);
  }
  wait(&status);

}

void randomShift()
{
  system("randshuf=$(shuf -en 1 *.jpg | cut -d . -f 1); printf 'Grape Kun akan menjaga : %s\n' $randshuf");
}

void makeDir()
{
  system("mkdir HewanDarat");
  system("mkdir HewanAir");
  system("mkdir HewanAmphibi");
}

void moveAnimal(char *dirre){
  system("mv *darat* HewanDarat/");
  system("mv *air* HewanAir/");
  system("mv *amphibi* HewanAmphibi/");
}

void zipAnimal()
{
  int status;
  pid_t zip_id = fork();

  if ((zip_id = fork()) == 0)
  {
    char *args[] = {"zip", "-r","-q","HewanDarat.zip", "HewanDarat", NULL};
    execv("/bin/zip", args);
  }else if((zip_id = fork()) == 0) {
    char *args[] = {"zip", "-r","-q", "HewanAir.zip","HewanAir", NULL};
    execv("/bin/zip", args);
  }else{
    char *args[] = {"zip", "-r","-q", "HewanAmphibi.zip","HewanAmphibi", NULL};
    execv("/bin/zip", args);
  }
  wait(&status);

}

int main()
{
  download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
  unzip("binatang.zip");
  randomShift();
  makeDir();
  moveAnimal(".");
  zipAnimal();
}
