## Soal 1
#### Pada persoalan ini, Grape-Kun akan melakukan penjagaan terhadap hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. 

A. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

**Download**
```c
void download(char *url, char *output)
{
  int status;
  pid_t download_id = fork();

  if (download_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (download_id == 0)
  {
    char *args[] = {"wget",url, "-q", "-O", output, NULL};
    execv("/bin/wget", args);
  }
  wait(&status);
}

```
Untuk function download, kita lakukan fork untuk menghasilkan child process. Child process akan kita gunakan untuk download dengan menggunakan `wget`.
`execv("/bin/wget", args);` digunakan untuk menjalankan proses wget dengan argumen args.

**Unzip**
```c
void unzip(char *input)
{
  int status;
  pid_t unzip_id = fork();

  if (unzip_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (unzip_id == 0)
  {
    char *args[] = {"unzip", "-q", input, NULL};
    execv("/bin/unzip", args);
  }
  wait(&status);

}
```
Untuk function unzip, kita lakukan fork untuk menghasilkan child process. Child process akan kita gunakan untuk download dengan menggunakan `unzip`.
`execv("/bin/wget", args);` digunakan untuk menjalankan proses wget dengan argumen args.


B. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

Random Shift
```c
void randomShift()
{
  system("randshuf=$(shuf -en 1 *.jpg | cut -d . -f 1); printf 'Grape Kun akan menjaga : %s\n' $randshuf");
}
```
Untuk melakukan random, kita gunakan function `system(shuf -en 1)`. Agar kita hanya mengeluarkan nama hewan saja (tanpa .jpg), kita gunakan `cut -d .` 

C. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.


Make Directory
```c
void makeDir()
{
  system("mkdir HewanDarat");
  system("mkdir HewanAir");
  system("mkdir HewanAmphibi");
}
```
Untuk make directory, kita gunakan `system("mkdir (nama_file)")`

Move Animal
```c
void moveAnimal(char *dirre){
  system("mv *darat* HewanDarat/");
  system("mv *air* HewanAir/");
  system("mv *amphibi* HewanAmphibi/");
}
```
Untuk memindahkan file berdasarkan nama file, kita gunakan `system("mv *nama* *tujuan directory*")`.

D. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Zip
```c
void zipAnimal()
{
  int status;
  pid_t zip_id = fork();

  if ((zip_id = fork()) == 0)
  {
    char *args[] = {"zip", "-r","-q","HewanDarat.zip", "HewanDarat", NULL};
    execv("/bin/zip", args);
  }else if((zip_id = fork()) == 0) {
    char *args[] = {"zip", "-r","-q", "HewanAir.zip","HewanAir", NULL};
    execv("/bin/zip", args);
  }else{
    char *args[] = {"zip", "-r","-q", "HewanAmphibi.zip","HewanAmphibi", NULL};
    execv("/bin/zip", args);
  }
  wait(&status);

}

```
Untuk function zip, kita lakukan fork untuk menghasilkan child process. Child process akan kita gunakan untuk download dengan menggunakan `zip`.
`execv("/bin/wget", args);` digunakan untuk menjalankan proses wget dengan argumen args.

Main

```c
int main()
{
  download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
  unzip("binatang.zip");
  randomShift();
  makeDir();
  moveAnimal(".");
  zipAnimal();
}

```
Function main kita gunakan untuk memanggil semua function seperti `download` `unzip` `randomShift` `makeDir` `moveAnimal` dan `zipAnimal`

## Soal 2<br>

Pada persoalan ini, kita diminta untuk mendownload sebuah foto setiap 5 detik hingga 15 foto. Lalu untuk setiap 30 detik, akan dibuat satu folder baru lagi yang berisi foto setiap 5 detik hingga 15 foto juga. Hal ini akan berjalan secara daemon (background). Kemudian, kita diminta untuk membuat sebuah killer program dalam bahasa c yang memuat 2 mode, yaitu:
- mode A : menghentikan semua proses.
- mode B : berhenti membuat proses baru lagi dan membiarkan proses yang sudah dibuat untuk berjalan hingga selesai.

Foto-foto dapat didownload memiliki resolusi sebesar (time()%1000)+50 pada link https://picsum.photos/ 
Kemudian, untuk penamaan file menggunakan format timestamp pada saat file didownload. Hal ini juga berlaku pada penamaan folder.

Untuk setiap folder yang sudah berisi 15 foto, folder tersebut akan dizip dan folder akan dihapus.

Langkah-langkah yang dilakukan adalah:
1. Membuat function yang return timestamp dengan format %Y-%m-%d_%H:%M:%S.
```c
char* getTimestamp(){
    time_t cur = time(NULL);
    char* timestamp = malloc(20 * sizeof(char));
    struct tm* cur_time = localtime(&cur);

    strftime(timestamp, 20, "%Y-%m-%d_%H:%M:%S", cur_time);

    return timestamp;
}
```
2. Membuat function untuk membuat folder.<br>Untuk membuat folder, kita perlu menggunakan fungsi execv, namun dikarenakan setelah menggunakan fungsi tersebut, baris kode setelah nya tidak akan berjalan. Sehingga, kita perlu membuat sebuah child process dimana child process inilah yang akan menjalankan fungsi tersebut.
```c
char* create_folder(char* timestamp){
    char *folder_name = malloc(30 * sizeof(char));
    sprintf(folder_name, "%s", timestamp);
    
    pid_t make_folder = fork();
    if(make_folder == 0){
        char *args[] ={"mkdir", timestamp, NULL};
        execv("/bin/mkdir", args);
    }
    else{
        wait(NULL);
    }
    return folder_name;
}

```
3. Membuat fungsi untuk mendowload file dan zip<br>
Fungsi ini melakukan beberapa hal yang berbeda, yaitu:
    1. Membuat folder dengan fungsi 2.
    ```c
    char *timestamp = getTimestamp();
    char *folder_name = create_folder(timestamp);
    ```
    2. Mendowload foto setiap 5 detik hingga 15 foto. Untuk mendowload foto kita perlu menggunakan execv. Sama seperti sebelumnya, kita perlu membuat child process.
    Kita juga perlu membuat parent process yang berjalan untuk menunggu child process selesai.
    Menggunakan sleep(5) untuk memastikan untuk mendowload setiap 5 detik.
    ```c
    for(int i=0; i<15; i++){
        char filename[200];
        char *file_timestamp = getTimestamp();
        sprintf(filename, "%s/%s.jpg", folder_name, file_timestamp);

        char url[100];
        time_t cur = time(NULL);
        int image_size = (cur%1000)+50;
        sprintf(url, "https://picsum.photos/%d", image_size);
        pid_t download = fork();
        if(download == 0){
            char *args[] ={"wget", "-qO", filename, url, NULL};
            execv("/usr/bin/wget", args);
            printf("Failed\n");
        }
        else{
            wait(&status);
        }

        sleep(5);
    }
    ```
    3. Setelah child process mendowload 15 foto. Kita perlu menzip folder tersebut. Hal ini juga menggunakan execv, sehingga perlu membuat child process lagi.
    ```c
     pid_t zip = fork();

    if(zip == 0){
        char zip_file[100];
        sprintf(zip_file, "%s.zip", folder_name);
        char *args[] = {"zip", zip_file, folder_name, NULL};
        execv("/usr/bin/zip", args);
    }
    else{
        wait(&status);
    }
    ```
    4. Selanjutnya perlu menhapus folder. Bagian ini juga menggunakan execv, sehingga perlu membuat child process lagi.
    ```c
        pid_t remove_folder = fork();
        if(remove_folder == 0){
            char *args[] = {"rmdir", "-r", folder_name, NULL};
            execv("/bin/rm", args);
        }
        else{
            wait(&status);
        }
    ```

    Kemudian langkah-langkah tersebut disimpan pada sebuah fungsi.
    ```c
    void download_file_and_zip()
    ```

4.Membuat killer program dalam bentuk c.<br>
Pada bagian ini, kita ibarat seperti menulis baris kode c namun dengan menggunakan fprintf.
```c
    int status;
    FILE *pkiller = fopen("killer.c", "w");
    char string_format[] = "%s";

    fprintf(pkiller, "#include <stdio.h>\n#include <stdlib.h>\n#include <time.h>\n#include <unistd.h>\n#include <sys/types.h>\n#include <string.h>\n#include <sys/wait.h>\n#include <sys/stat.h>\n");
```
Tedapat 2 mode killer:
- mode A, untuk mematikan semua process kita perlu menggunakan pkill dengan kata kunci file output yang dijalankan oleh lukisan.c . 
- mode B, kita hanya perlu mematikan parent yang membuat child process(child yang mendowload foto-foto).Hal ini dapat dilakukan dengan kill -9 pid dari parent tersebut.

Pkill dan kill dapat dijalankan menggunakan execv, sehingga kita perlu membuat child process.
```c
    fprintf(pkiller, "int main(int argc, int *argv[]){\n");
    fprintf(pkiller, "pid_t child = fork();\n"
                    "if(child == 0){\n");
    fprintf(pkiller, "if(strcmp(argv[1], \"-a\") == 0){char *args[] = {\"pkill\", \"-9\", \"lukisan\", NULL};\n"
                    "execv(\"/usr/bin/pkill\", args);}\n");

    fprintf(pkiller, "else if(strcmp(argv[1], \"-b\") == 0){char *args[] = {\"kill\", \"-9\", \"%d\", NULL};\n", parent_id);
    fprintf(pkiller, "execv(\"/bin/kill\", args);}\n}");
    fprintf(pkiller, "}\n");
```
Fungsi tersebut kami namankan:
```c
void generate_killer(int argc, char *argv[], int parent_id)
```
Parent_id untuk mode B.

5. Membuat fungsi yang menjalankan killer program.<br>
Sebelum menjalankan, kita perlu memcompile killer program tersebut. Hal ini dapat menggunakan execv yang membutuhkan child process.
```c
    int status;
    pid_t child = fork();
    if(child == 0){
        char *args[] = {"gcc", "killer.c", "-o", "killer"};
        execv("/usr/bin/gcc", args);
    }
    else{
        wait(&status);
    }
```
Lalu kita dapat menjalankan output setelah memcompile killer program tersebut (menggunakan execv yang membutuhkan child process).
```c
    pid_t run_output = fork();    
    if(run_output == 0){
        char *args[] = {argv[0], argv[1], NULL};
        execv("./killer", args);
    }
    else{
        wait(&status);
    }
```
Setelah dirun kita perlu menghapus file killer.c dan output c file tersebut.
```c
    pid_t delete = fork();
    if(delete == 0){
        char *args[] = {"rm", "killer.c", NULL};
        execv("/bin/rm", args);
    }
    else{
        char *args[] = {"rm", "killer", NULL};
        execv("/bin/rm", args);
    }
```

6. Membuat main yang dapat menerima 1 atau 2 input.
- Satu input

Dikarenakan proses pembuatan folder, download dan zip dilakukan setiap 30 detik. Kita harus membuat child process untuk membagi tugas antara parent dan child.
Parent akan memanggil fungsi download_zip setiap 30 detik. Sedangkan, child akan menjalankan proses tersebut.
Sebelum itu kita perlu memanggil generate_killer dengan memberikan pid parent yang akan membuat child terus-menerus.
Lalu untuk menjalankan secara background, parent membuat sebuah child process yang dimana parent akan exit setelah child dibuat.
```c
if(argc == 1){
        pid_t child = fork();
        if(child == 0){
            generate_killer(argc, argv, getpid());
            while (1){
                pid_t download_zip= fork();
                if(download_zip == 0){
                    download_file_and_zip();
                    exit(EXIT_SUCCESS);
                }
                else{
                    sleep(30);
                }
            }
        }
        else{
            exit(EXIT_SUCCESS);
        }
    }
```

- Dua input

Hal ini mengidentifikasikan program untuk kill process yang dihasilkan berdasarkan mode yang diinginkan.\
```c
else if(argc == 2 && (strcmp(argv[1], "-a") == 0) ||    (strcmp(argv[1], "-b") == 0)){
        run_killer(argc, argv);
        return 0;
    }   
```
## Soal 3<br>
#### Pada persoalan ini, Kita harus membantu ten hag mendownload folder zip dari gdrive, lalu memilah pemain manchester united dan membaginya sesuai posisi, dan terakhir menentukan formasi.

A. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
```c
    CURL *curl;
    FILE *fp;
    CURLcode res;
    char url[] = "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF";
    char outfilename[] = "players.zip";

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(outfilename,"wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        curl_easy_cleanup(curl);
        fclose(fp);
    }
```
Pertama untuk medownload file gdrive kami menggunakan curl yang disediakan oleh library curl.h dan variable fp bertipe file untuk membuat file dengan nama player.zip.

```c
    pid_t child_id;
    int status;
    child_id = fork();

    if(child_id == 0){
        char *argv[] = {"unzip", "/home/dafarelcky/Prak_2_sisop/players.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    else {
        while ((wait(&status)) > 0);
        child_id = fork();
        if(child_id == 0){
        char *argv[] = {"rm", "/home/dafarelcky/Prak_2_sisop/players.zip", NULL};
        execv("/usr/bin/rm", argv);
        }
        else {
            while ((wait(&status)) > 0);
            manutd();
            kategori();
            buatTim(4, 3, 3);
        }
    }
```
Membuat fork untuk menjalankan 2 program karena execv nantinya akan menterminated segala program yang ada dibawahnya. Di parent process kami memberi wait yang gunanya untuk menunggu child menyelesaikan tugasnya terlebih dahulu. Sehingga yang terjadi adalah unzip dilakukan oleh exec di child process, lalu parent process melakukan remove folder zip dan menjalankan fungsi fungsi untuk perintah soal berikutnya.

B. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory. 

```c
void manutd (){
    DIR *dp;
    struct dirent *ep;
    char path[] = "/home/dafarelcky/Prak_2_sisop/players";
    char filename[50];
    dp = opendir(path);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            char path[] = "/home/dafarelcky/Prak_2_sisop/players/";
            strcpy(filename, ep->d_name);
            strncat(path, filename, 50);
          //  printf("%s\n", path);
            if (strstr(path, "ManUtd") == NULL){
            //    printf("Ada ");
                if(strstr(path, "png") != NULL) remove(path);
            }
          //  puts(ep->d_name);
        } 
    (void) closedir (dp);
    } else perror ("Couldn't open the directory");    
}
```
membuat variable dir untuk menyimpan folder yang akan di filter dan membuat string path yang menyimpan alamat folder. lalu buat loop untuk mengecek satu satu file yang ada di folder dp lalu satukan judul file dengan string path untuk mendapatkan path file dan cek jika string tidak memiliki string ManUtd dan file mempunyai kata png didalamnya(karena file . dan .. tidak dihapus) maka hapus file berdasarkan path tadi.

C. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
```c
    int n1 = fork();
	int n2 = fork();
    pid_t child_id;
    int status;
```
membuat 2 fork sehingga bisa menjalankan 4 program yang digunakan untuk masing masing posisi pemain dan int status untuk digunakan jika dibutuhkan wait
```c
if (n1 > 0 && n2 > 0) {
        child_id = fork();

        if(child_id == 0){
            char *argv[] = {"mkdir", "/home/dafarelcky/Prak_2_sisop/players/Kiper", NULL};
            execv("/usr/bin/mkdir", argv);
        }
        else {
            while ((wait(&status)) > 0);
            dp = opendir(path_a);
            if (dp != NULL)
            {
                
                while ((ep = readdir (dp))) {
                int len = strlen(ep->d_name);
                int des = strlen("/home/dafarelcky/Prak_2_sisop/players/Kiper/");
                int ph = strlen("/home/dafarelcky/Prak_2_sisop/players/");
                size_t size = (len*(len + 1)/2) + 1;
                size_t des_size = (des*(des + 1)/2) + 1;
                size_t path_size = (ph*(ph + 1)/2) + 1;
                char filename[size]; 
                char des_path[des_size];
                char path[path_size];
                strcpy(des_path, "/home/dafarelcky/Prak_2_sisop/players/Kiper/");
                strcpy(path, "/home/dafarelcky/Prak_2_sisop/players/");
                strcpy(filename, ep->d_name);
                strncat(path, filename, strlen(ep->d_name));
                strncat(des_path, filename, strlen(ep->d_name));
```
Pertama buat fork lagi untuk menjalankan 2 program karena 1 program akan digunakan untuk execv untuk membuat folder kiper. Di parent process kami mengecek satu satu file didalam folder lalu disetiap loopnya menghitung panjang file, panjang destinasi file, dan panjang asal file karena akan dilakukan perhitungan size = (len*(len + 1)/2) + 1 untuk membuat char baru yang menyimpan destinasi dan asal path ditambah dengan nama file agar tidak terjadi segmentation fault. lalu lakukan strcat dan strcpy untuk memindahkan char dengan path + filename ke variable string yang baru
```c
if (strstr(ep->d_name, "Kiper") != NULL && strstr(ep->d_name, "png")!= NULL){
                    fp1 = fopen(path, "rb");
                    fp2 = fopen(des_path, "wb");

                    while ((ch = fgetc(fp1)) != EOF) {
                        fputc(ch, fp2);
                    }
                    fclose(fp1);
                    fclose(fp2);
                    remove(path);
                }
                } 
                (void) closedir (dp);
            } else perror ("Couldn't open the directory");
        }
	}
```
Lalu cek satu satu dengan strstr apakah string memiliki kata kata yang dicari dalam hal ini "Kiper" dan cek apakah file memiliki substring png untuk menghindari pemindahan file "." dan "..". Setelah itu open file fp1 dan fp2 dengan fp1 sebagai yang akan dipindah dan fp2 sebagai file yang akan dituju lalu pindahkan isi file dengan loop memakai fget dan fput. Dan akhirnya tutup kedua file dan hapus file awal

Kami melakukan cara yang sama persis untuk 3 process lainnya untuk bek, gelandang dan penyerang.

D. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
```c
void buatTim(int Bek, int Gelandang, int Penyerang){
    FILE *fp;
    char name[] = "/home/dafarelcky/Formasi_";
    char temp[1];
    printf("%s\n", name);
    sprintf(temp, "%d", Bek);
    strcat(name, temp);
    printf("%s\n", name);
    strcat(name, "-");
    printf("%s\n", name);
    sprintf(temp, "%d", Gelandang);
    strcat(name, temp);
    printf("%s\n", name);
    strcat(name, "-");
    printf("%s\n", name);
    sprintf(temp, "%d", Penyerang);
    strcat(name, temp);
    printf("%s\n", name);
    strcat(name, ".txt");
    printf("%s\n", name);
    fp = fopen(name, "w");
```
Pertama tama, buat string yang menyimpan path dan file name yang diminta soal, kami menggunakan sprintf untuk mengambil isi variable int dan menambahkannya ke variable name menggunakan strcat

```c
Node* pq = newNode("fill", 0);
```
Kami membuat priority queue untuk menyimpan filenya.
Berikut adalah code priority queuenya:
```c
typedef struct node {
	char data[100];
	int priority;

	struct node* next;

} Node;
Node* newNode(char *d, int p)
{
	Node* temp = (Node*)malloc(sizeof(Node));
    strcpy(temp->data, d);
	temp->priority = p;
	temp->next = NULL;

	return temp;
}
const char* peek(Node** head)
{
	return (*head)->data;
}
void pop(Node** head)
{
	Node* temp = *head;
	(*head) = (*head)->next;
	free(temp);
}
void push(Node** head, char *d, int p)
{
	Node* start = (*head);
	Node* temp = newNode(d, p);
	if ((*head)->priority < p) {
		temp->next = *head;
		(*head) = temp;
	}
	else {
		while (start->next != NULL &&
			start->next->priority > p) {
			start = start->next;
		}
		temp->next = start->next;
		start->next = temp;
	}
}
int isEmpty(Node** head)
{
	return (*head) == NULL;
}
```

```c
    DIR *dp;
    struct dirent *ep;
    char path[] = "/home/dafarelcky/Prak_2_sisop/players/Kiper";
    char filename[50];
    dp = opendir(path);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            strcpy(filename, ep->d_name);
            char stat[2];
            int c = 0;
            for (int i = strlen(filename)-6; i < strlen(filename) - 4; i++)
            {
                stat[c] = filename[i];
                c++;
            }
            stat[c] = '\0';
            int num_stat = atoi(stat);
            strcat(filename, "\n");
```
Untuk mendapatkan data data yang dibutuhkan untuk dimasukkan ke priority queue yaitu int rating pemain dan string filename pemain, kami membuat string sepanjang 2 dengan mengambil dari string filename dari index ke 6 terakhir sampai ke 4 terakhir karena format filenya 6 index [rating sepanjang 2 char].png. Setelah itu buat variable int untuk menyimpan rating tadi setelah diconvert ke int menggunakan atoi lalu tambahkan \n di string agar saat di print akan memberikan baris baru

```c
if (strstr(filename, "png")!= NULL) push(&pq, filename, num_stat);
```
Jika string memiliki substring png maka insert ke priority queue dengan data string filename dan integer rating

```c
fputs(peek(&pq), fp);
        while (!isEmpty(&pq)) {
		 //   printf("%s\n", peek(&pq));
		    pop(&pq);
	    }
    (void) closedir (dp);
    } else perror ("Couldn't open the directory");
```
Untuk penyelesaian pindahkan isi dari priority queue ke dalam file yang baru sesuai dengan jumlah yang diminta menggunakan fputs dan kosongkan list priority queue

Untuk bek, gelandang, dan penyerang kami menggunakan metode yang sama persis perbedaan hanya terdapat di pemindahan isi priority queue ke file fp karena yang dipindah jumlahnya harus sesuai yang diminta. Seperti contoh untuk Bek:
```c
for (int i = 0; i < Bek; i++)
       {
            fputs(peek(&pq_b), fp);
            pop(&pq_b);
       }
```

## Soal 4<br>
Pada persoalan ini, kita diminta untuk membuat sebuah program c yang berjalan seperti cron job.
Program yang dibuat hanya dapat menerima 4 jenis inputan, yaitu jam(0-24), menit(0-59), detik(0-60),'*' yang artinya any, dan sebuah path file .sh .

Pada kode terdapat beberapa langkah yang dilakukan, yaitu:

1. Memvalidasi input dari user.
- Pertama, mengecek jumlah input yang masuk dengan menggunakan argc.
```c
    if(argc != 5){
            printf("Argument are not valid\n");
            exit(EXIT_FAILURE);
        }
```
- Kedua, memvalidasi inputan jam.
Disini kami menggunakan dua variabel hour dan star_h untuk mengecek inputan jam. Star_h akan menerima '*', sedangkan hour akan menerima inputan berupa angka (0-23).
```c
    int hour = 0;  
    char star_h[2] = " ";

    if(strcmp(argv[1], "*") == 0){
        strcpy(star_h, "*");
    }
    else if(isdigit(argv[1][0]) && (atoi(argv[1]) >= 0 &&  atoi(argv[1]) <= 23)){
        hour = atoi(argv[1]);
    }
    else{
        printf("Invalid hour\n");
        exit(EXIT_FAILURE);
    }
```

- Ketiga, memvalidasi inputan menit.
Sama seperti sebelumnya, kami menggunakan dua variabel min  dan star_m untuk mengecek inputan menit. star_m akan menerima '*', sedangkan min akan menerima inputan berupa angka (0-59).
```c
    int min = 0;
    char star_m[2] = " ";

    if(strcmp(argv[2], "*") == 0 ){
            strcpy(star_m, "*");
        }
        else if(isdigit(argv[2][0]) && atoi(argv[2]) >= 0 &&  atoi(argv[2]) <= 59){
            min = atoi(argv[2]);
        }
        else{
            printf("Invalid minute\n");
            exit(EXIT_FAILURE);
        }

```

- Keempat, memvalidasi inputan detik.
Sama seperti sebelumnya, kami menggunakan dua variabel sec dan star_s untuk mengecek inputan jam. star_s akan menerima '*', sedangkan sec akan menerima inputan berupa angka (0-59).

```c
    int sec = 0;
    char star_s[2] = " ";
    if(strcmp(argv[3], "*") == 0 ){
        strcpy(star_s, "*");
    }
    else if(isdigit(argv[3][0]) && (atoi(argv[3]) >= 0 &&  atoi(argv[3]) <= 59)){
        sec = atoi(argv[3]);
    }
    else{
        printf("Invalid second\n");
        exit(EXIT_FAILURE);
    }
```

- Kelima, memvalidasi inputan file .sh
Pertama-tama, kami mengecek apakah file tersebut telah dibuat. Lalu, memberi peringatan berupa error untuk file yang tidak ada/tidak dapat diakses, serta apakah file tersebut adalah sebuah bash file.
```c
    if(access(argv[4], F_OK) == 0){
        if(strstr(argv[4], ".sh") == NULL){
            printf("File is not a bash file\n");
            exit(EXIT_FAILURE);
        }
    }

     else if(access(argv[4], F_OK) != 0){
        if(errno == ENOENT){
            printf("File doesnt exist\n");
        }
        else if(errno == EACCES){
            printf("File unaccessable\n");
        }
        else{
            printf("Unknown error\n");
        }
        exit(EXIT_FAILURE);
    }
```
- Keenam, kami membuat program sleep hingga waktu yang sudah ditentukan untuk menghemat penggunaan cpu. Berikut adalah penghitungan interval waktu sekarang hingga waktu yang yang sudah ditentukan.
```c
    int sleep_time = 0;

    if(strcmp(star_h, "*") == 0){
        if(strcmp(star_m, "*") == 0){
            if(strcmp(star_s, "*") != 0){
                sleep_time = sec - cur_sec;

                 if(sec < cur_sec){
                    sleep_time += 60;
                 }
            }
        }
        else{
            sleep_time = min * 60 + sec - (cur_min * 60 + cur_sec);
            if(min < cur_min){
                sleep_time += 3600;
            }
        }
    }

    else{
        sleep_time = (hour * 3600 + min * 60 + sec) - (cur_hour * 3600 + cur_min * 60 + cur_sec);
        if(hour < cur_hour){
            sleep_time += (24 * 3600);
        }
    }

    if(sleep_time < 0){
        sleep_time = 0;
    }
```

- Ketujuh, untuk membuat program dapat dirun di background, pertama kita membuat sebuah parent yang memiliki child (fork), selanjutnya parent ini akan di exit terlebih dahulu, agar child process akan di run hingga dikill melalui terminal. (Orphan Process).

```c
    pid_t child = fork();
    if(child == -1){
        exit(EXIT_FAILURE);
    }
    else if(child == 0){
    
    }
    else{
        exit(EXIT_SUCCESS);
    }
```

- Selanjutnya, untuk menjalankan program bash pada waktu yang ditentukan, perlu menggunakan fungsi execvp dimana fungsi ini akan terminate code setelahnya dan berpindah ke bash yang ditentukan. 
Oleh karena itu, agar file bash ini tetap berjalan, kita perlu memanggil execvp pada sebuah child process dimana process yang diterminate adalah child process bukan process aslinya. Namun sebelum menjalankan execvp, kita perlu mengecek apakah interval waktu sekarang dan interval yang kita input sebelumnya sudah cocok
```c
    current_time = time(NULL);
    local_time = localtime(&current_time);

    cur_hour = local_time->tm_hour;
    cur_min = local_time->tm_min;
    cur_sec = local_time->tm_sec;

    int status;


    pid_t child_id  = fork();
    if(child_id == -1){
        exit(EXIT_FAILURE);
    }

    else if(child_id == 0){
        if((hour == cur_hour || strcmp(star_h, "*") == 0 ) && (min == cur_min || strcmp(star_m, "*") == 0 ) && (sec == cur_sec || strcmp(star_s, "*") == 0 )){
            char *args[] = {"/bin/bash", argv[4], NULL};
            execvp("/bin/bash", args);
        }   
    }
```

- Lalu, child akan dibuat terus-menerus dengan menggunakan while() dan wait() agar tidak adanya tabrakan.

-Terakhir, membuat program sleep lagi dengan kalkukasi dibawah.
```c
    sleep_time = 0;

    if(strcmp(star_h, "*") != 0 ){
        sleep_time += (24* 3600);
    }

    else if(strcmp(star_m, "*") != 0 ){
        sleep_time += 3600;
    }

    else if(strcmp(star_s, "*") != 0 ){
        sleep_time += 60;
    }

    sleep(sleep_time);
```


Menangani special case

- \* min \*  or hour \* \*

Agar bash dapat berjalan dengan tepat waktu, kita perlu membuat sebuah variable bool first untuk menindikatorkan interval waktu yang perlu dilewati terlebih dahulu. 
Contoh : Current time = 03 05 57    Input user  = \* 05 \*
maka bash file akan berjalan sekali dan menunggu hingga 04 05 00 untuk berjalan lagi.
Dengan itu terdapat waktu yang lebih singkat ketika dibanding dengan file bash yang dirun pada waktu 04 05 57.
Hal ini hanya terjadi di awal saja.
Berikut adalah cari untuk mencari nilai sleep yang paling optimal.
```c
    if(first == true){
        if(strcmp(star_h, "*") != 0){
            if(strcmp(star_m, "*") == 0){ 

                
                sleep_time -= cur_min*60;
            }
            if(strcmp(star_s, "*") == 0){
                sleep_time -= cur_sec;
            }
        }
        else{
            if(strcmp(star_m, "*") != 0){
                if(strcmp(star_s, "*") == 0){
                    sleep_time -= cur_sec;
                }
            }
        }
        first = false;
    }
```
